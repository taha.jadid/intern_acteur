package tahadeta.example.internacteur

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper.loop
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import androidx.core.view.isVisible

class dashboard<textview : View?> : AppCompatActivity() {
    @SuppressLint("WrongViewCast")

    lateinit var view1: ConstraintLayout
    lateinit var view2: ConstraintLayout
    lateinit var view3: ConstraintLayout
    lateinit var view4: ConstraintLayout
    lateinit var view5: ConstraintLayout
    lateinit var view6: ConstraintLayout
    lateinit var view7: ConstraintLayout
    lateinit var view8: ConstraintLayout
    lateinit var view9: ConstraintLayout

    var viewsAreVisible: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        val afficher = findViewById<TextView>(R.id.textView30)
        val carte_sim = findViewById<ImageView>(R.id.imageView4)
// set on-click listener
        carte_sim.setOnClickListener {
            val intent = Intent(applicationContext, activation_carte_sim::class.java)
            startActivity(intent) }

        bindView()

        afficher.setOnClickListener {

            if (viewsAreVisible) {
                goneViews()
                viewsAreVisible = false
            } else {
                showView()
                viewsAreVisible = true
            }

        }
    }

    private fun showView() {
        view1.visibility = View.VISIBLE
        view2.visibility = View.VISIBLE
        view3.visibility = View.VISIBLE
        view4.visibility = View.VISIBLE
        view5.visibility = View.VISIBLE
        view9.visibility = View.VISIBLE
        view8.visibility = View.VISIBLE
        view7.visibility = View.VISIBLE
        view6.visibility = View.VISIBLE
    }

    private fun bindView() {

        view1 = findViewById(R.id.constraintLayout2)
        view2 = findViewById<ConstraintLayout>(R.id.constraintLayout)
        view3 = findViewById<ConstraintLayout>(R.id.constraintLayout1)
        view4 = findViewById<ConstraintLayout>(R.id.constraintLayout4)
        view5 = findViewById<ConstraintLayout>(R.id.constraintLayout5)
        view6 = findViewById<ConstraintLayout>(R.id.constraintLayout7)
        view7 = findViewById<ConstraintLayout>(R.id.constraintLayout8)
        view8 = findViewById<ConstraintLayout>(R.id.constraintLayout9)
        view9 = findViewById<ConstraintLayout>(R.id.constraintLayout10)
    }

    private fun goneViews() {
        view9.visibility = View.GONE
        view8.visibility = View.GONE
        view7.visibility = View.GONE
        view6.visibility = View.GONE
        view5.visibility = View.GONE
        view4.visibility = View.GONE
    }

}




