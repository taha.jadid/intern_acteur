package tahadeta.example.internacteur

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val nb = findViewById<EditText>(R.id.editTextNumber)
        val text2 = nb.text.toString()

        val back = findViewById<ImageView>(R.id.imageView2)
        // set on-click listener
        back.setOnClickListener {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
        }

        // passage a la page suivante

        val suivant = findViewById<Button>(R.id.button3)
        suivant.setOnClickListener {
            val intent = Intent(applicationContext, dashboard::class.java)
            startActivity(intent)
            finish()
        }
    }
}
