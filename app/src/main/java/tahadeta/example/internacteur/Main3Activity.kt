package tahadeta.example.internacteur





import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*




class Main3Activity : AppCompatActivity() {


    override public fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)
        val edt1 = findViewById<EditText>(R.id.editTextNom)
        val edt2 = findViewById<EditText>(R.id.editTextPrenom)
        val edt3 = findViewById<EditText>(R.id.editTextEmail)
        val edt4 = findViewById<EditText>(R.id.editTextTextCIN)

        val Boutton_Confirmer = findViewById<Button>(R.id.ButtonConfirmer)

        val Boutton_Back = findViewById<ImageView>(R.id.img_back)



        Boutton_Confirmer.setOnClickListener() {

            if (edt1.length() == 0) {


                edt1.setError("Veuillez Entrer votre Nom Svp !!")

            } else if (edt2.length() == 0) {


                edt2.setError("Veuillez  Entrer votre Prénom Svp !!")
            } else if (edt3.length() == 0) {


                edt3.setError("Veuillez  Entrer votre Adresse Mail Svp !!")
            } else if (edt4.length() == 0) {


                edt4.setError("Veuillez Entrer votre Numéro de carte d'identité Svp !!")
            } else {

                val intent = Intent(this, Main4Activity::class.java)
                startActivity(intent)
            }


        }
        Boutton_Back.setOnClickListener(){

            val intent = Intent(applicationContext,MainActivity::class.java)
            startActivity(intent)

        }
    }

    fun clickImage(view: View) {}

}