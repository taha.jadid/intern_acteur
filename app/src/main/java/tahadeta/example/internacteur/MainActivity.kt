package tahadeta.example.internacteur

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val number = findViewById<EditText>(R.id.editTextPhone)
        val passwordAuthentication = findViewById<EditText>(R.id.editTextTextPassword)
        var test1 ="123"
        var test2 ="123"

        // passage a la page suivante

        val button = findViewById<Button>(R.id.button)
        val bt = findViewById<Button>(R.id.button2)

        button.setOnClickListener {
            val text = number.text.toString()
            val textS = passwordAuthentication.text.toString()

            if ((text == test1) && (textS == test2)) {
                val intent = Intent(applicationContext, MainActivity2::class.java)
                startActivity(intent)
                finish()
            } else {
                var counter = 3
                counter--
                if (counter == 0) {
                    button.isEnabled = false
                }
            }
        }

        bt.setOnClickListener(){

            val intent=Intent(applicationContext,Main3Activity::class.java)
            startActivity(intent)
        }
    }
}
