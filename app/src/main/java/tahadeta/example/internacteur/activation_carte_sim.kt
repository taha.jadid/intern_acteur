package tahadeta.example.internacteur

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView

class activation_carte_sim : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation_carte_sim)
        val prenom = findViewById<EditText>(R.id.editTextTextPersonName)
        val fist_name = prenom.text.toString()
        val nom = findViewById<EditText>(R.id.editTextTextPersonName1)
        val last_name = nom.text.toString()
        val num = findViewById<EditText>(R.id.editTextTextPersonName3)
        val CIN = num.text.toString()
        val mdn_num = findViewById<EditText>(R.id.editTextPhone2)
        val MDN = mdn_num.text.toString()
        val puk_num = findViewById<EditText>(R.id. editTextPhone3)
        val puk = puk_num.text.toString()
        val back2 = findViewById<ImageView>(R.id.imageView8)
        // set on-click listener
        back2.setOnClickListener {
            val intent = Intent(applicationContext, dashboard::class.java)
            startActivity(intent)
        }
        // passage a la page suivante
        val suivant2 = findViewById<Button>(R.id.button4)
        suivant2.setOnClickListener {
            val intent = Intent(applicationContext,activation_carte_sim2::class.java)
            startActivity(intent)
            finish()
        }
    }
}