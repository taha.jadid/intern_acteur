package tahadeta.example.internacteur

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity_repertoire_creationAM1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_repertoire_creation_am1)


        val bt1 : Button = findViewById<Button>(R.id.ButtonConfirmer)
        bt1.setOnClickListener(){

            val intent = Intent(applicationContext,MainActivity_repertoire_creation_am2::class.java)
            startActivity(intent)


        }
    }

    fun clickImage(view: View) {}
}