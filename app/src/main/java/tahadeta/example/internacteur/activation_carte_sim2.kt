package tahadeta.example.internacteur

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class activation_carte_sim2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation_carte_sim2)
        val back3 = findViewById<ImageView>(R.id.imageView15)
        // set on-click listener
        back3.setOnClickListener {
            val intent = Intent(applicationContext,activation_carte_sim::class.java)
            startActivity(intent)
        }

        val acanner_button = findViewById<Button>(R.id.button5)
        acanner_button.setOnClickListener {
            val intent = Intent(applicationContext,dashboard::class.java)
            startActivity(intent)
            finish()
        }
        val back_button = findViewById<Button>(R.id.button6)
        back_button.setOnClickListener {
            val intent = Intent(applicationContext,dashboard::class.java)
            startActivity(intent)
            finish()
        }
    }
}